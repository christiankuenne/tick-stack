# TICK Stack

Hello world example for TICK stack. It's still only TIC, no Kapacitor yet.

### Getting Started

```
docker-compose up -d
```

This will start the 3 containers for InfluxDB, Telegraf and Chronograf. InfluxDB will create an initial database _telegraf_, an admin user and a telegraf user. Telegraf will push some standard metrics to InfluxDB. I also added an input plugin to collect the CPU temperature. If the temperature is not correct, amend the following line in `telegraf.conf` (change the thermal zone):

```
commands = ["awk '{print $1/1000}' /sys/class/thermal/thermal_zone0/temp"]
```

Chronograf is running on port _8888_.
